angular.module('cday.services', ['firebase'])

.service('AuthService', ['$rootScope', '$http', '$state', '$log', '$firebaseAuth', function($rootScope, $http, $state, $log, $firebaseAuth) {
    
    var auth = this;

    var firebaseAuth = $firebaseAuth();

    var callbacks = [];
    
    var currentUser = angular.fromJson(window.localStorage['currentUser']);
    
    $rootScope.$on('$stateChangeStart', function(event, next, current) {
        
        if (next.url !== "/login") {
            if (!isLoggedIn()) {
                $state.transitionTo('login');
                event.preventDefault();
            }
        }
        
    });
    
    function getUserInfo() {
        $log.info(currentUser);
    };
    
    function addCallback(callback) {
        callbacks.push(callback);
    };
    
    function login(user) {
        
        $log.debug(user);
        
        return firebaseAuth.$signInWithEmailAndPassword(user.email, user.password)
            .then(function(response) {
            
                saveCurrentUser();
            
                console.log(currentUser);
            
                onLogin();
            
                return response;
            
            });
        
    };
    
    function logout() {
        
        $log.debug(firebase.auth().currentUser);
        
        currentUser = null;
        
        window.localStorage['currentUser'] = null;
        
        return firebaseAuth.$signOut();
        
    }
    
    function register(user) {
        $log.debug(user);
        return firebaseAuth.$createUserWithEmailAndPassword(user.email, user.password)
            .then(function(response) {
            
                getUserInfo();
            
                return response;
            
            });
    };
    
    function getUid() {
        
        if (!currentUser) {
            saveCurrentUser();
        }
        
        var uid;
        
        if (currentUser) {
            uid = currentUser.uid;
        }
        else {
            uid = null;
        }
        
        return uid;
        
    };
    
    function saveCurrentUser() {
        
        currentUser = firebase.auth().currentUser;
        window.localStorage['currentUser'] = angular.toJson(currentUser);
        
    };
    
    function isLoggedIn() {
        
        
        if (currentUser) {
            return true;
        }
        else {
            return false;
        }
        
    };

    function onLogin() {
        
        angular.forEach(callbacks, function(callback) {
            callback();
        })
        
    };
    
    function getRandomId() {
        return Math.random() * 20000;
    };

    return {
        
        addCallback: addCallback,
        
        login: login,
        
        register: register,
        
        getUserInfo: getUserInfo,
        
        logout: logout,
        
        getUid: getUid,
        
        isLoggedIn: isLoggedIn,

        getFirstName: function() {

            var prom = loadUserDetails(currentUser.id)
                .then(function() {
                    return currentUser.firstName;
                })
            return prom;
            
        },

        getIdFromLocalStorage: function() {

            var id = window.localStorage['userId'];
            console.log("User id is " + currentUser.id);
            //return currentUser.id;
            return id;

        },

        tasksUpdated: function() {

            return currentUser.tasksUpdated;
            
        },
        
        checkTasksUpdate: function() {
              
            return loadUserDetails(currentUser.id)
                .then(function() {
                    
                });
            
        },

        onTasksUpdate: function() {
                
            do { var value = getRandomId(); } while (value == currentUser.tasksUpdateId);

            return $http.put(getUrlForId(currentUser.id), { 'tasksUpdateId': value })
                .then(function(response){

                    console.log(response);
                    return loadUserDetails(currentUser.id);

                });

        },
        
        onScheduleUpdate: function(startTime, finishTime) {
                
            do { var value = getRandomId(); } while (value == currentUser.scheduleUpdateId);

            window.localStorage['startTime'] = startTime;
            window.localStorage['finishTime'] = finishTime;

            $http.put(getUrlForId(currentUser.id), { 'scheduleUpdateId': value, 'startTime': startTime, 'finishTime': finishTime })
                .then(function(response) {

                    $log.debug('Here comes the schedule response');
                    $log.debug(response);

                    return loadUserDetails(currentUser.id);

                })
                
        }
        
    }

}]);