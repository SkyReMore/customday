angular.module('cday.services')

.service('ScheduleGenerationService', ['$log', function($log) {
    
    var sGService = this;
    
    function setTasks(tasks) {
        sGService.tasks = tasks;
    };
    
    function setTimes(start, finish) {
        // Must remove start of hour later, when minutes and seconds inputs will be ready
        sGService.startTime = start;
        sGService.finishTime = finish;
    };
    
    function precisionOf(a) {
      if (!isFinite(a)) return 0;
      var e = 1, p = 0;
      while (Math.round(a * e) / e !== a) { e *= 10; p++; }
      return p;
    };
    
    function getPrecision() {
        
        var maxPrecision = 0;
        
        for (var i = 0; i < sGService.tasks.length; i++) {
            
            var task = sGService.tasks[i],
            precision = precisionOf(task.priority);
            if (precision > maxPrecision) {
                maxPrecision = precision;
            }
            
        }
        
        return maxPrecision;
        
    };
    
    // Temporary function
    
    function getTimeOffset(minutes) {
        
        var hours = Math.floor(minutes / 60);
        var minutes = minutes % 60;
        
        var date = new Date();
        
        date.setHours(0);
        date.setMinutes(0);
        var start = date.getTime();
        
        date.setHours(hours);
        date.setMinutes(minutes);
        var offset = date.getTime() - start;
        
        return offset;
        
    };
    
    // Temporary function
    
    function createTaskProbabilityRanges(precision) {
        
        var ranges = [];
        
        var last = 0;
        
        for (var i = 0; i < sGService.tasks.length; i++) {
            var task = sGService.tasks[i];
            
            if (task.priority !== 0) {
                var range = {id: task.$id, start: (last + 1 / Math.pow(10, precision)), end: (last + task.priority)};

                last = range.end;

                ranges.push(range);
            }
        }
        
        console.log(ranges);
        
        return ranges;
        
    };
    
    function randomSelectTasks(probabilityRanges, precision) {
        
        var selectedTasks = [];
        
        var i = sGService.startTime;
        
        var selectedRange;
        // When task repeats more than one time the multiplier increases the of next repeat
        var repeatMultiplier = 1;
        while (i < sGService.finishTime) {
            
            var selectedTask;
            
            var randomNum = selectedRange ? Math.random() * (probabilityRanges[probabilityRanges.length - 1].end +
                                                             (selectedRange.end - selectedRange.start + 1) * repeatMultiplier) : Math.round(Math.random() * probabilityRanges[probabilityRanges.length - 1].end);
            
            for (var a = 0; a < probabilityRanges.length; a++) {
                var range = probabilityRanges[a];
                
                if (randomNum >= range.start && randomNum <= range.end) {
                    selectedRange = range;
                    selectedTask = angular.copy(sGService.tasks.$getRecord(range.id));

                    if (!selectedTask) {
                        $log.error('No task found');
                        return;
                    }
                    
                    repeatMultiplier = 1;
                    
                    break;
                }
                else if (a === probabilityRanges.length - 1) {
                    selectedTask = angular.copy(sGService.tasks.$getRecord(range.id));
                    
                    repeatMultiplier++;
                    
                    break;
                }
            }
            selectedTask.startTime = i;
            i += selectedTask.time - 1;
            selectedTask.finishTime = i;
            i++;

            selectedTasks.push(selectedTask);
        }
        
        return selectedTasks;
        
    };
    
    function insertRequiredTasks(selectedTasks) {
        
        for (var i = 0; i < sGService.tasks.length; i++) {
            var task = sGService.tasks[i];
            
            if (task.required) {
                task = angular.copy(task);
                randomTime = sGService.startTime + Math.round(Math.random() * (sGService.finishTime - sGService.startTime - task.time));
                
                task.startTime = randomTime;
                task.finishTime = randomTime + task.time;
                insertTask(selectedTasks, task);
            }
            
        }
        
        return selectedTasks;
        
    };
    
    function insertTask(selectedTasks, task) {
        
        for (var i = 0; i < selectedTasks.length && selectedTasks[i].startTime < task.finishTime; i++) {
            var selectedTask = selectedTasks[i];
            
            var startInside = false;
            var finishInside = false;
            
            // Hit in the range
            if (selectedTask.startTime <= task.startTime && selectedTask.finishTime >= task.startTime) {
                startInside = true;
            }
            if (selectedTask.startTime <= task.finishTime && selectedTask.finishTime >= task.finishTime) {
                finishInside = true;
            }
            
            // Inside range
            if (startInside && finishInside) {
                //Split on two parts
                
                var secondPart = angular.copy(selectedTask);
                secondPart.startTime = task.finishTime + 1;
                secondPart.finishTime = selectedTask.finishTime;
                selectedTasks.splice(i + 1, 0, task);
                selectedTasks.splice(i + 2, 0, secondPart);

                selectedTask.finishTime = task.startTime - 1;
                
                break;
                
            }
            // Only start inside
            else if (startInside) {
                selectedTask.finishTime = task.startTime - 1;
            }
            // Only finish inside
            else if (finishInside) {
                selectedTask.startTime = task.finishTime + 1;
                selectedTasks.splice(i, 0, task);
                break;
            }
            // Selected inside of required
            else if (selectedTask.startTime >= task.startTime && selectedTask.finishTime <= task.finishTime) {
                selectedTasks.splice(i, 1);
                i--;
            }
            
            if (i === selectedTasks.length - 1) {
                selectedTasks.push(task);
            }     
        }
        
        console.log(task);
    };
    
    function prepareTasks(tasks) {
        
        for (var i = 1; i < tasks.length; i++) {
            var task = tasks[i];
            var previousTask = tasks[i - 1];
            if (task.$id === previousTask.$id) {
                previousTask.finishTime = task.finishTime;
                tasks.splice(i, 1);
                i--;
            }
            if (task.startTime > sGService.finishTime) {
                tasks.splice(i, 1);
                i--;
            }
            else if (task.finishTime > sGService.finishTime) {
                task.finishTime = sGService.finishTime;
            }
            if (task.startTime >= task.finishTime) {
                $log.error("Task start time is more or equal to its finish time");
                tasks.splice(i, 1);
                i--;
            }
        }
        
        return tasks;
        
    };
    
    function generate() {
        var precision = getPrecision();
        
        var probabilityRanges = createTaskProbabilityRanges(precision);

        var generatedTasks = prepareTasks(insertRequiredTasks(randomSelectTasks(probabilityRanges, precision)));
        
        return generatedTasks;
    };
    
    return {
        setTasks: setTasks,
        setTimes: setTimes,
        generate: generate
    }
    
}]);