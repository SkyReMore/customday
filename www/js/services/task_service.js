angular.module('cday.services')

.service('TaskService', ['$http', '$rootScope', 'AuthService', '$firebaseArray', function($http, $rootScope, AuthService, $firebaseArray) {

    var taskService = this;
    
    taskService.state = { tasksLoaded : false };
    
    var database = firebase.database();
    
    var tasksRef;
    var tasks;
    
    var callbacks = [];
    
    AuthService.addCallback(initArray);
    
    if (AuthService.isLoggedIn()) {
        initArray();
    }
    
    function initArray() {
        
        tasksRef = database.ref("/tasks/" + AuthService.getUid());
        tasks = $firebaseArray(tasksRef);
        
        tasks.$loaded()
            .then(function() {

                taskService.state.tasksLoaded = true;

                onTasksUpdate();
                tasks.$watch(onTasksUpdate);

            });
        
    };
    
    // Only for developping!
    
    function changeTimeFormat() {
        if(!taskService.state.tasksLoaded) {
            for (var i = 0; i < tasks.length; i++) {
                var task = tasks[i];

                task.time = task.time / 1000;

                tasks.$save(task);
            }
        }
    }
    
    // Only for developping!
    
    function addCallback(callback) {
        callbacks.push(callback);
    };
    
    function onTasksUpdate() {
        angular.forEach(callbacks, function(callback) {
            console.log("Update!");
            callback(tasks);
        });
    };
    
    function isTasksLoaded() {
        return taskService.state.tasksLoaded;
    };
    
    function saveTask(task) {
        
        tasks.$add(task)
            .then(function(ref) {
                
                console.log(ref.key);
            
            });
        
    };
    
    function loadTasks() {
        return tasks;
    };
    
    function getKey(task) {
        return tasks.$$getKey(task);
    };
    
    function getRecordCopy(key) {
        return angular.copy(tasks.$getRecord(key));
    };
    
    function update(object) {
        
        var idx = tasks.$indexFor(object.$id);
        
        tasks[idx] = object;
        tasks.$save(idx);
        
    };
    
    function change(task, key) {
        
        for (var i = 0; i < tasks.length; i++) {
            if (key === tasks[i].key) {
                tasks[i] = task;
            }
        }
             
    };

    function getUrlForId(id) {
        return getUrl() + id;
    };

    return {
        
        isTasksLoaded: isTasksLoaded,
        
        saveTask: saveTask,
        
        loadTasks: loadTasks,

        getAllTasks: function(id) {
            return loadTasks(id);
        },

        getKey: getKey,
        
        getRecordCopy: getRecordCopy,
        
        update: update,

        delete: function(id) {
            return $http.delete(getUrlForId(id))
                .then(function() {
                    AuthService.onTasksUpdate();
                });
        },
        
        addCallback: addCallback

    }
    
}])