angular.module('cday.services')

.service('ScheduleService', ['$http', '$q', '$rootScope', '$firebaseArray', '$firebaseObject', 'AuthService', 'TaskService', function($http, $q, $rootScope, $firebaseArray, $firebaseObject, AuthService, TaskService) {
    
    var scheduleService = this;
    
    scheduleService.state = {tasksLoaded : false};
    
    scheduleService.info = {};
    
    var database = firebase.database();
    
    var uid;
    
    var tasks;
    
    var callbacks = [];
    var scheduleExceptionsCallback;
    
    AuthService.addCallback(recieveData);
    
    if (AuthService.isLoggedIn()) {
        recieveData();
    };
    
    function recieveData() {
        
        uid = AuthService.getUid();
        
        loadScheduleInfo();
        
        var scheduleRef = database.ref(getDataPath() + '/schedule');
        tasks = $firebaseArray(scheduleRef);
      
        createLoadingCallback();
        
    };
    
    function loadScheduleInfo() {
        var infoRef = database.ref(getDataPath() + "/info");
        scheduleService.info = $firebaseObject(infoRef);
    };
    
    function createLoadingCallback() {
        
        tasks.$loaded()
            .then(function() {
            
                scheduleService.state.tasksLoaded = true;
            
                onScheduleArrayUpdate();

            });
        
    };
    
    function getDataPath() {
        return "/schedules/" + uid;
    };
    
    function addCallback(callbackFunction) {
        callbacks.push(callbackFunction);
    };
    
    function addScheduleExceptionsCallback(callbackFunction) {
        scheduleExceptionsCallback = callbackFunction;
    };
    
    function onScheduleArrayUpdate() {
        angular.forEach(callbacks, function(callback) {
            callback(getScheduledTasks());
        });
    };
    
    function isTasksLoaded() {
        return scheduleService.state.tasksLoaded;
    };
    
    function create(newTasks, expirationTime, excetptionCallback) {
        
        angular.forEach(tasks, function(task) {
            tasks.$remove(task)
                .then(function() {
                })
                .catch(function() {
                    scheduleExceptionsCallback('Failed to remove item from old schedule.');
                });
        });
        
        angular.forEach(newTasks, function(task) {
            
            var scheduledTask = {};
            
            scheduledTask.key = TaskService.getKey(task);
            scheduledTask.startTime = task.startTime;
            scheduledTask.finishTime = task.finishTime;
            
            tasks.$add(scheduledTask)
                .then(function() {
                })
                .catch(function() {
                    scheduleExceptionsCallback('Failed to add an item into new schedule.');
                });
            
        });
        
        makeLocalTasksCopy(tasks);
        
        setScheduleExpirationTime(expirationTime);
    };
    
    function getCurrentTime() {
        return moment().unix();
    };
    
    function getScheduleExpirationTime() {
        return scheduleService.info.scheduleExpirationTime ? 
            scheduleService.info.scheduleExpirationTime : window.localStorage['scheduleExpirationTime'];
    };
    
    function setScheduleExpirationTime(time) {
        scheduleService.info.scheduleExpirationTime = time;
        scheduleService.info.$save();
        window.localStorage['scheduleExpirationTime'] = time;
    };
    
    function getScheduledTasks() {
        var scheduledTasks = [];
        
        if (isTasksLoaded() && (getCurrentTime() < getScheduleExpirationTime())) {
            var savedTasks = tasks;
            
            if (!savedTasks) {
                savedTasks = angular.fromJson(window.localStorage['scheduledTasks']);
            }
            else {       
                for (var i = 0; i < savedTasks.length; i++) {
                    var item = savedTasks[i];

                    if (item.startTime) {

                        var task = TaskService.getRecordCopy(item.key);
                        task.startTime = item.startTime;
                        task.finishTime = item.finishTime;

                        scheduledTasks.push(task);
                    }
                }   
            }
        }
        return scheduledTasks;
    };
    
    function getUserTimes() {
        var times = {};
        
        times.startTime = parseInt(window.localStorage[uid + 'startTime']) || moment().unix();
        times.finishTime = parseInt(window.localStorage[uid + 'finishTime']) || moment().add(16, 'h').unix();
        
        return times;
    };
    
    function saveUserTimes(startTime, finishTime) {
        window.localStorage[uid + 'startTime'] = moment.duration(moment.unix(startTime)._d).asSeconds();
        window.localStorage[uid + 'finishTime'] = moment.duration(moment.unix(finishTime)._d).asSeconds();
    };
    
    function makeLocalTasksCopy(tasks) {
        
        tasksCopy = [];
        
        angular.forEach(tasks, function(task) {
            
            var copy = {};
            copy.key = task.key;
            copy.startTime = task.startTime;
            
            tasksCopy.push(copy);
            
        });
        
        window.localStorage['scheduledTasks'] = angular.toJson(tasksCopy);
    };
    
    function refresh() {
        recieveData();
    };

    return {
        addCallback: addCallback,
        addScheduleExceptionsCallback: addScheduleExceptionsCallback,
        
        isTasksLoaded: isTasksLoaded,
        
        getScheduledTasks: getScheduledTasks,
        
        getUserTimes: getUserTimes,
        saveUserTimes: saveUserTimes,
        
        create: create,
        
        refresh: refresh
    }
    
}]);