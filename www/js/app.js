// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

angular.module('cday', [
    'ionic', 
    'cday.controllers', 'cday.directives', 'cday.services',
    'ngMessages'
    ])


.run(function($ionicPlatform, $rootScope, $state, AuthService, TaskService) {
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
          // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
          // for form inputs)
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

          // Don't remove this line unless you know what you are doing. It stops the viewport
          // from snapping when text inputs are focused. Ionic handles this internally for
          // a much nicer keyboard experience.
          cordova.plugins.Keyboard.disableScroll(true);
        }
        if(window.StatusBar) {
          StatusBar.styleDefault();
        }

        var isMobile = !(ionic.Platform.platforms[0] == "browser");
    
    });

    function unauthorize() {

        AuthService.logout();
        console.log("user is unauthorized");
      
    };

    $rootScope.$on('unauthorized', function() {
        unauthorize();
    });

})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
  .state('menu', {
    url: '/menu',
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'MenuCtrl'
  })
  .state('menu.schedlist', {
    url: '/schedlist',
    views: {
      'menu-content': {
        templateUrl: "templates/schedlist.html",
        controller: 'SchedListCtrl'
      }
    },
    cache: false
  })
  .state('menu.optlist', {
    url: '/optlist',
    views: {
      'menu-content': {
        templateUrl: "templates/optlist.html",
        controller: 'OptListCtrl as optl'
      }
    }
  })
  .state('menu.newtask', {
    url: '/newtask',
    views: {
      'menu-content': {
        templateUrl: "templates/newtask.html",
        controller: 'NewTaskCtrl'
      }
    },
    cache: false
  })
  .state('menu.edittask', {
    url: '/edittask/:taskKey',
    views: {
      'menu-content': {
        templateUrl: "templates/edittask.html",
        controller: 'EditTaskCtrl'
      },
    },
    cache: false
  })
  .state('login', {
    url: '/login',
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl',
    params: { error: null }
  });
  
  $urlRouterProvider.otherwise(function($injector) {
    var $state = $injector.get("$state");
    $state.go("menu.schedlist");
  });
  
})
