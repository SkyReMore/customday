angular.module('cday.directives', [])

.directive('formNameInput', function() {
    
    return {
        
        templateUrl: "templates/directives/form-name-input.html"
        
    }
    
})

.directive('formDescriptionInput', function() {
    
    return {
        
        templateUrl: "templates/directives/form-description-input.html"
        
    }
    
})

.directive('formTimeInput', function() {
    
    return {
        
        templateUrl: "templates/directives/form-time-input.html"
        
    }
    
})

.directive('formPriorityInput', function() {
    
    return {
        
        templateUrl: "templates/directives/form-priority-input.html"
        
    }
    
})

.directive('textarea', function() {
    
    return {
        
        restrict: 'E',
        link: function(scope, elem, attr) {
            
            var onTextEnter = function() {
                
                elem.css("height", "auto");
                var height = elem[0].scrollHeight;
                elem.css("height", height + "px");
                
            }
            
            scope.$watch(attr.ngModel, function() {
                onTextEnter();
                console.log("Textarea is updated");
            })
            
        }
    }
    
})

.directive('formattedTime', function() {
    
    return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ngModel) {
            if (!ngModel) {
                return;
            }
            if (attrs.type !== 'time') {
                return;
            }
            ngModel.$formatters.unshift(function(value) {
                return value.replace(/:[0-9]+.[0-9]+$/, '');
            });
        }
    };
    
})