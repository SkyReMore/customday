angular.module('cday.controllers')

.controller('EditTaskCtrl', function($scope, $state, TaskService) {

    $scope.taskKey = $state.params.taskKey;

    $scope.data = TaskService.getRecordCopy($scope.taskKey);
    
    $scope.data.time = moment().startOf('day').second($scope.data.time)._d;
    
    if (!$scope.data.required) {
        $scope.data.required = false;
    }

    //$scope.data.time = 0;

    $scope.updateTask = function(data) {
        var taskData = angular.copy(data);
        
        if ($scope.data.time) {
            taskData.time = moment.duration($scope.data.time).asSeconds();
        }

        TaskService.update(taskData);
       
        $state.go('menu.optlist');

    }

})
