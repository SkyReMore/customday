angular.module('cday.controllers', [])

.controller('MenuCtrl', function($scope, $state, AuthService, ScheduleService) {

    $scope.refresh = function() {
        console.log("Refreshing");
        ScheduleService.refresh();
    };
    
    $scope.logout = function() {
        //$rootScope.$broadcast('unauthorized');
        AuthService.logout();
    };
    
    $scope.isState = function(name) {
        return $state.is(name);
    };

})
