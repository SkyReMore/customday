angular.module('cday.controllers')

.controller('NewTaskCtrl', function($scope, $state, AuthService, TaskService) {

    $scope.data = {};
    
    $scope.data.required = false;

    $scope.createTask = function(data) {
        var taskData = angular.copy(data);
        
        if ($scope.data.time) {
            taskData.time = moment.duration($scope.data.time).seconds();
        }
    
        TaskService.saveTask(taskData);
        
        $state.go("menu.optlist");

    }

})