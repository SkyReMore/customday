angular.module('cday.controllers')

.controller('SchedListCtrl', ['$scope', 'AuthService', 'TaskService', 'ScheduleService', 'ScheduleGenerationService', '$ionicLoading', '$ionicPopup', function($scope, AuthService, TaskService, ScheduleService, ScheduleGenerationService, $ionicLoading, $ionicPopup) {

    //
    /// Auto-generated stuff
    //
    
    $scope.state = {};

    $scope.state.isNewSchedule = false;
    
    $scope.data = {};
    
    showLoading();

    (function getData() {
        
        // User's times

        var times = ScheduleService.getUserTimes();
    
        $scope.data.startTime = {}, $scope.data.finishTime = {};
        
        $scope.data.startTime = moment().startOf('day').seconds(times.startTime)._d;
        $scope.data.finishTime = moment().startOf('day').seconds(times.finishTime)._d;
        
        // User's tasks
        $scope.tasks = TaskService.loadTasks();
        $scope.state.tasksLoaded = TaskService.isTasksLoaded();
        
        // If user go to the page from another one, or somehow schedule recieved earlier than controller is loaded
        $scope.scheduledTasks = ScheduleService.getScheduledTasks();
        $scope.state.scheduledTasksLoaded = ScheduleService.isTasksLoaded();
        
        checkReadiness();
        
    })();
    
    TaskService.addCallback(onTasksLoading);
    ScheduleService.addCallback(onScheduleLoading);
    ScheduleService.addScheduleExceptionsCallback(onScheduleException);
    
    function onTasksLoading(tasks) {
        
        setTasks(tasks);
        $scope.state.tasksLoaded = true;
        checkReadiness();
      
    };
    
    function onScheduleLoading(tasks) {
        
        setScheduledTasks(tasks);
        $scope.state.scheduledTasksLoaded = true;
        checkReadiness();
      
    };
    
    function onScheduleException(message) {
        $ionicPopup.alert({
            title: 'Please check your network connection',
            template: message
        });
    };
    
    function setTasks(tasks) {
        $scope.tasks = tasks;
    };

    function setScheduledTasks(tasks) {
        $scope.scheduledTasks = tasks;
    };
    
    function checkReadiness() {
        if ($scope.state.tasksLoaded && $scope.state.scheduledTasksLoaded) {
            hideLoading();
        }
    }

    //
    /// Schedule functions
    //

    function stickAll(tasks) {

        var toDelete = [];

        for (var i = 0; i < tasks.length - 1; i++) {
            console.log(tasks[i].name);
            if (tasks[i].name == tasks[i + 1].name) {
                toDelete.push(i + 1);
            }
        }

        for (var i = 0; i < toDelete.length; i++) {
            tasks.splice(toDelete[i], 1);
        }

    }

    function setNewScheduleTimes() {
        var startTime = moment($scope.data.startTime).unix();
        var finishTime = moment($scope.data.finishTime).unix();
        
        if (startTime > finishTime) {
            finishTime = moment($scope.data.finishTime).add(1, 'd').unix();
        }
        
        $scope.data.newScheduleStartTime = startTime;
        $scope.data.newScheduleFinishTime = finishTime;
        
        console.log("New schedule start");
        console.log($scope.data.newScheduleStartTime);
        console.log("New schedule finish");
        console.log($scope.data.newScheduleFinishTime);
    };

    //
    /// Schedule creation
    //
    
    $scope.createSchedule = function() {
        
        if ($scope.data.startTime && $scope.data.finishTime) {    
            ScheduleGenerationService.setTasks($scope.tasks);

            // Maybe should them to the start of minute for safety. 
            // There can be some s/ms input on android, even after I hide them with the directive
            setNewScheduleTimes();

            ScheduleGenerationService.setTimes($scope.data.newScheduleStartTime, $scope.data.newScheduleFinishTime);

            $scope.scheduledTasks = ScheduleGenerationService.generate();

            if ($scope.scheduledTasks) {
                $scope.state.isNewSchedule = true;
            }
        }
        
    }

    $scope.saveSchedule = function(scheduledTasks) {
        
        ScheduleService.create(scheduledTasks, $scope.data.newScheduleFinishTime);
        
        ScheduleService.saveUserTimes($scope.data.newScheduleStartTime, $scope.data.newScheduleFinishTime);
        
        $scope.state.isNewSchedule = false;
    }

    $scope.getFormattedTime = function(time) {
        return moment.unix(time).format('hh:mm a');
    };
    
    //
    /// UI functions
    //

    function showLoading() {
        
        $ionicLoading.show({ template: "Loading..." })
            .then(function() {
            
            });
        
    };
    
    function hideLoading() {
        $ionicLoading.hide()
            .then(function() {
                console.log("Indicator is now hidden!");
            });
    };

}]);