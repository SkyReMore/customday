angular.module('cday.controllers')

.controller('OptListCtrl', function($scope, $rootScope, AuthService, TaskService, $ionicLoading) {
    
    $scope.tasks = TaskService.loadTasks();
    
    if (!$scope.tasks.length) {
        showLoading();
    }
    else {
        $scope.percents = getTotalPercents($scope.tasks);
    }
    
    TaskService.addCallback(setTasks);
    
    function setTasks(tasks) {
        
        $scope.tasks = tasks;
        $scope.percents = getTotalPercents(tasks);
        hideLoading();
        
    };
    
    function getTotalPercents(tasks) {
        
        var percents = 0;
        
        if (tasks) {
            console.log(tasks.length);
            for (var i = 0; i < tasks.length; i++) {
                percents += tasks[i].priority;
            }
        }
        
        return percents;

    };

    $scope.deleteTask = function(id) {

        TaskService.delete(id)
        .then(function() {

            console.log("Task deleted successfully");

        },
        function(error) {

            console.log(error);
            console.log("Failed to delete a task");

        })

    };
    
    function showLoading() {
        
        $ionicLoading.show({ template: "Loading..." })
            .then(function() {
                console.log("Indicator is successfully shown!");
            });
        
    };
    
    function hideLoading() {
        $ionicLoading.hide()
            .then(function() {
                console.log("Indicator is now hidden!");
            });
    };

})
