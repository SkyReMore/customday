angular.module('cday.controllers')

.controller('LoginCtrl', function($scope, $rootScope, $state, AuthService) {

    $scope.data = {};

    $scope.error = $state.params.error;

    $scope.authenticate = function() {

        $scope.error = null;
        $scope.success = null;

        if ($scope.data.newAccount) {
            $scope.signup($scope.data);
        }
        else {
            $scope.signin($scope.data);
        }

    };

    $scope.signup = function(user) {

        console.log(user.email);
        console.log(user.password);

        AuthService.register(user)
            .then(function(response) {
                $scope.signin(user);
            }, showError);

        };

    $scope.signin = function(user) {

        AuthService.login(user)
            .then(function(response) {
                
                console.log(response);
                $state.go('menu.optlist');

            }, showError);

    }

    $scope.onLogin = function() {

        $rootScope.$broadcast('authorized');
        $state.go('menu.schedlist');
    }

    function showError(error) {
        $scope.error = error && error.data || error.error_description || 'Unknown error from server';
    }

    //$scope.socialSignin

})